﻿# Universidad Distrital Francisco José de Caldas
## Diseño arquitectural de software y patrones

### Miembros del grupo
- Karen Milena Pinilla Delgado - 20141020073
- Edwar Díaz Ruiz - 20141020004
- Daissi Bibiana Gonzalez Roldan - 20152020108
- Gabriel Vargas Monroy - 20141020107

### Arquitectura a Usar
#### Justificación

Dentro del documento gestaremos la implementacion de un proceso mas adaptativo y creemos que la mejor seleccion de los mismos sera desde un punto de vista generado por **micro-servicios** con una variacion aplicada al desarrollo futuro del mismo, desde el punto de vista de la implementacion el desarrollo del proceso se hace a traves de lo que se puede entender como un API pero que con la construccion de la definicion de micro-servicios podemos ahora abordar y es la construccion dirigida a un **Bus de servicio Empresarial (ESB)**, esto brinda una construccion basada en la arquitectura y que por ende podemos gestar como interesante, aplicable y tratable

Su comportamiento es identico al de un API, este posee configuracion y su propio balanceo, ademas posee una arquitectura tipica para diseño ademas de las miles de implementaciones que a la hora de diseñar

#### Definicion

##### Bus de Servicio Empresarial (ESB)

Conforme las organizaciones van moviéndose hacia las arquitecturas orientadas a servicios, se dan cuenta que están trabajando con un “mix” entre lo nuevo y lo viejo. Un ESB es básicamente la integración de lo nuevo y lo viejo, brindándo un lugar central para los servicios, las aplicaciones, y recursos de TI en general que se desean conectar. En otras palabras, lo que se busca es una infraestructura y un sistema de eventos que me permitan conectar cualquier recurso de TI sin importar la tecnología que utiliza el recurso. Esta infraestructura debería permitir administrar los cambios en los requerimientos sin causar problemas a los servicios ya instalados. Esta infraestructura debe de ser confiable y robusta. Aquí es donde entra el concepto de ESB.

Un ESB no solamente permite combinar y re ensamblar servicios, sino que también debe permitir conectar nuevas aplicaciones, servicios web y cualquier otro tipo de aplicaciones tales como aplicaciones LOB ( Line of Business ), archivos batch, legacy middleware a través de adaptadores; todo esto manteniendo la abstracción del manejo de mensajes a través del patrón publicar-suscribir.

Siendo más concretos, podemos decir que un ESB ofrece las siguientes funcionalidades:

- Transparencia de Ubicación: El ESB ayuda a desligar el consumidor del servicio de la ubicación del proveedor del servicio. El ESB provee una plataforma central para comunicarse con cualquier aplicación requerida sin ligar el recibidor del mensaje con el que envía el mensaje.
- Conversión de Protocolo de Transporte: Un ESB debe de tener la capacidad de integrar de forma transparente a través de diferentes protocolos de transporte tales como HTTP(s), JMS, FTP, SMTP, TCP, etc.
- Transformación de Mensaje: El ESB brinda funcionalidad para transformar mensajes desde un formato hasta otro formato basado en estándares tales como XSLT y XPath.
- Ruteo de Mensajes: El ESB determina el destino de los mensajes entrantes.
- Mejora del Mensaje: El ESB puede brindar funcionalidad para agregar información faltante basado en los datos del mensaje de entrada.
- Seguridad: Autenticación, autorización, y funcionalidad de encriptación se proveen a través del ESB para asegurar los mensajes entrantes. Igualmente estas funcionalidades se aplican a mensajes salientes para satisfacer requerimientos de seguridad del proveedor del servicio a consumir.
- Monitoreo y Administración: Un ambiente de monitoreo y administración del ESB es fundamental para configurar el ESB para que sea confiable y tenga un alto desempeño; al mismo tiempo, nos permite monitorear la ejecución de los mensajes y su flujo dentro del ESB.

![Arquitectura ESB](image/esb_architecture.png "Arquitectura de ESB")

##### Arquitectura dirigida a Micro-Servicios APlicando RESTfull

Una “arquitectura de microservicios” es un enfoque para desarrollar una aplicación software como una serie de pequeños servicios, cada uno ejecutándose de forma autónoma y comunicándose entre sí, por ejemplo, a través de peticiones HTTP a sus API.

Normalmente hay un número mínimo de servicios que gestionan cosas comunes para los demás (como el acceso a base de datos), pero cada microservicio es pequeño y corresponde a un área de negocio de la aplicación.

Además cada uno es independiente y su código debe poder ser desplegado sin afectar a los demás. Incluso cada uno de ellos puede escribirse en un lenguaje de programación diferente, ya que solo exponen la API (una interfaz común, a la que le da igual el lenguaje de programación en la que el microservicio esté programado por debajo) al resto de microservicios.

No hay reglas sobre qué tamaño tiene que tener cada microservicio, ni sobre cómo dividir la aplicación en microservicios, pero algunos autores como Jon Eaves caracterizan un microservicio como algo que a nivel de código podría ser reescrito en dos semanas.

Todo ello bajo un Modelo implementable y desarrollable facilmente.


![components micro](image/components.jpg "Componentes de Micro-servicios")
*Modelo de Referencia*

![Modelo de Implementacion micro](image/impl_model.jpg "Modelo de Implementacion de micro-servicio")
*Modelo de Implementacion*

![Modelo de Despliegue micro](image/deploy_model_simple.jpg "Modelo de despliegue de micro-servicio")
*Modelo de Despliegue**

#### Lineamientos

- **Identificación de recursos:** toda aplicación REST debe poder identificar sus recursos de manera uniforme. HTTP implementa esto usando las llamadas URIs (Uniform resource identifier). Esta es la URL que usamos tradicionalmente, y aunque hay una diferencia sutil entre URLs y URIs [2], diremos que toda URL es una URI a su vez. Esta identificación del recurso no incluye la representación del mismo, cosa que veremos a continuación.
- **Recursos y representaciones:** visto que todo recurso debe tener una identificación (URI), REST define también la manera en que podemos interactuar con la representación del mismo, ya sea para editarlo o borrarlo, directamente del servidor. Estas representaciones se dejan a instancias de la implementación final del programa, pero HTTP define distintas cabeceras de tipos, y un contenido en la respuesta, por lo que nuestras aplicaciones pueden enviar el contenido en el formato que quieran, siempre y cuando este contenido contenga la información necesaria para poder operar con el objeto en el caso de que tengamos permiso para hacerlo.
- **Mensajes autodescriptivos:** cuando hacemos peticiones a un servidor, éste debería devolver una respuesta que nos permita entender sin lugar a duda cual ha sido el resultado de la operación, así como si dicha operación es cacheable, si ha habido algún error, etc. HTTP implementa esto a través del estado y una serie de cabeceras. El cómo se usan estos estados y cabeceras depende por entero de la implementación de nuestro programa, en otras palabras, REST no fuerza el contenido de dichos elementos, por lo que el programa que se ejecuta en el servidor, y que en última instancia accede a los recursos y opera con ellos, tiene la responsabilidad de devolver estados y cabeceras que se correspondan con el estado real de la operación realizada. Esto es importante tenerlo en cuenta, ya que, desgraciadamente, un gran número de aplicaciones y servicios web no respetan esta regla (por lo tanto no pueden ser considerados REST), lo que nos lleva a tener que realizar todo tipo de workarounds y cosas por el estilo. En la segunda parte de esta serie veremos un caso práctico de un servicio web que no respeta esta norma, y daremos varias soluciones posibles.
- **HATEOAS:** por último, y algo que la mayoría de servicios web no cumplen, es la necesidad de incluir en las respuestas del servidor toda aquella información que necesita el cliente para seguir operando con este servicio web. En otras palabras, el cliente no tiene porqué saber que cuando obtenemos, por ejemplo, un objeto cualquiera, tenemos además las opciones de modificarlo, o eliminarlo. El servidor debe enlazar a estas operaciones en la respuesta a dicha petición. De esta manera, lo único que necesita saber un cliente sobre una aplicación REST, es el punto de entrada (endpoint). Además nos garantiza más independencia entre el cliente y el servidor. Desgraciadamente, este último principio no se implementa en la mayoría de APIs que usamos hoy en día, por lo que, siendo estrictos, podríamos decir que la mayoría de servicios web no son 100% RESTful. No obstante, esto es una limitación menor que en prácticamente ningún caso supone un problema.


#### Potencialidad

Cuando se construye un modelo basado en un ESB podemos usar diferentes arquitecturas y lenguajes en cada una, no importa tampoco el protocolo del mensaje, este se va a caracterizar en ser adaptado para ser configurado, administrado y gestionado directamente desde este.

### Modelos

<details>
 <summary><h4>Modelos Basicos</summary>

<details>
 <summary><h5>Modelo Introductorio</summary>
<img src="CapaNegocio/CapaIntroductoria/image/modelo.png" >
</details>

---

<details>
 <summary><h5>Modelo Organizacional</summary>
<img src="CapaNegocio/CapaOrganizacional/image/ViewPointOrg.png" >
</details>

---

</details>

---

<details>
 <summary><h4>Modelos de Negocio</summary>

<details>
 <summary><h5>Modelo de Cooperacion</summary>
<img src="CapaNegocio/CapaCooperacion/image/ViewPointCoop.png" >
</details>

---
<details>
 <summary><h5>Modelo de Vista de Funciones</summary>

---

<details>
<summary>Cierre de semestre</summary>
<img src="CapaNegocio/CapaFunciones/image/CierreSemestre.png" >
</details>

<details>
<summary>Prueba academica</summary>
<img src="CapaNegocio/CapaFunciones/image/PruebaAcademica.png" >
</details>

<details>
<summary>Paz y salvo</summary>
<img src="CapaNegocio/CapaFunciones/image/PazYSalvo.png" >
</details>

<details>
<summary>Proyecto de grado</summary>
<img <img src="CapaNegocio/CapaFunciones/image/ProyectoGrado.png" >
</details>

<details>
<summary>Generacion certificados</summary>
<img src="CapaNegocio/CapaFunciones/image/GeneracionCertificado.png" >
</details>

<details>
<summary>Actualizacion de datos (Generico)</summary>
<img src="CapaNegocio/CapaFunciones/image/ActualizacionDatos.png" >
</details>

<details>
<summary>Subir notas</summary>
<img src="CapaNegocio/CapaFunciones/image/SubirNotas.png" >
</details>

<details>
<summary>Derecho ECAES</summary>
<img src="CapaNegocio/CapaFunciones/image/DerechoECAES.png" >
</details>

<details>
<summary>Admitidos</summary>
<img src="CapaNegocio/CapaFunciones/image/Aspirante.png" >
</details>

<details>
<summary>Generacion recibos</summary>
<img src="CapaNegocio/CapaFunciones/image/GeneracionRecibos.png" >
</details>

<details>
<summary>Calificacion</summary>
<img src="CapaNegocio/CapaFunciones/image/Calificacion.png" >
</details>

<details>
<summary>Reliquidacion</summary>
<img src="CapaNegocio/CapaFunciones/image/Reliquidacion.png" >
</details>

<details>
<summary>Reportar pago</summary>
<img src="CapaNegocio/CapaFunciones/image/ReporteDePago.png" >
</details>

<details>
<summary>ver historia academica</summary>
<img src="CapaNegocio/CapaFunciones/image/HistoriaAcademica.png" >
</details>

<details>
<summary>Movilidad academica</summary>
<img src="CapaNegocio/CapaFunciones/image/MovilidadAcademica.png " >
</details>

<details>
<summary>Graduacion</summary>
<img src="CapaNegocio/CapaFunciones/image/Graduacion.png" >
</details>

<details>
<summary>Modificar notas</summary>
<img src="CapaNegocio/CapaFunciones/image/ModificarNotas.png " >
</details>

<details>
<summary>Pagos (No proceso)</summary>
<img src="CapaNegocio/CapaFunciones/image/Pagos.png" >
</details>

<details>
<summary>Reingresos</summary>
<img src="CapaNegocio/CapaFunciones/image/Reingresos.png" >
</details>

<details>
<summary>Congelados</summary>
<img src="CapaNegocio/CapaFunciones/image/Congelados.png" >
</details>

<details>
<summary>Eliminados</summary>
<img src="CapaNegocio/CapaFunciones/image/Eliminados.png" >
</details>

<details>
<summary>Ver notas</summary>
<img src="CapaNegocio/CapaFunciones/image/VerNotas.png" >
</details>

<details>
<summary>Gestion documentos (No proceso)</summary>
<img src="CapaNegocio/CapaFunciones/image/GestionDocumentos.png" >
</details>

<details>
<summary>Evaluacion docente (No proceso)</summary>
<img src="CapaNegocio/CapaFunciones/image/EvaluacionDocente.png" >
</details>

<details>
<summary>Inscripcion casos especiales</summary>
<img src="CapaNegocio/CapaFunciones/image/inscribir_casos_especiales.png" >
</details>

<details>
<summary>Soporte documental</summary>
<img src="CapaNegocio/CapaFunciones/image/verificacion-documental.png" >
</details>

<details>
<summary>Monitoria</summary>
<img src="CapaNegocio/CapaFunciones/image/monitoria.png" >
</details>

<details>
<summary>Evaluar fraude</summary>
<img src="CapaNegocio/CapaFunciones/image/fraude.png" >
</details>

<details>
<summary>Inscripcion</summary>
<img src="CapaNegocio/CapaFunciones/image/inscripcion.png" >
</details>

<details>
<summary>Preinscripcion</summary>
<img src="CapaNegocio/CapaFunciones/image/pre-inscripcion.png" >
</details>

<details>
<summary>Gestion calendario</summary>
<img src="CapaNegocio/CapaFunciones/image/calendario.png" >
</details>

</details>

---

<details>
<summary><h5>Modelo de Vista de Proceso</summary>

<details>
<summary>Cierre de semestre</summary>
<img src="CapaNegocio/CapaProcesos/image/CierreSemestre.png" >
</details>

<details>
<summary>Prueba academica</summary>
<img src="CapaNegocio/CapaProcesos/image/PruebaAcademica.png" >
</details>

<details>
<summary>Paz y salvo</summary>
<img src="CapaNegocio/CapaProcesos/image/PazYSalvo.png" >
</details>

<details>
<summary>Proyecto de grado</summary>
<img src="CapaNegocio/CapaProcesos/image/ProyectoGrado.png" >
</details>

<details>
<summary>Generacion certificados</summary>
<img src="CapaNegocio/CapaProcesos/image/GeneracionCertificado.png" >
</details>

<details>
<summary>Actualizacion de datos (Generico)</summary>
<img src="CapaNegocio/CapaProcesos/image/ActualizacionDatos.png" >
</details>

<details>
<summary>Subir notas</summary>
<img src="CapaNegocio/CapaProcesos/image/SubirNotas.png" >
</details>

<details>
<summary>Derecho ECAES</summary>
<img src="CapaNegocio/CapaProcesos/image/DerechoECAES.png" >
</details>

<details>
<summary>Aspirante y Admitidos</summary>
<img src="CapaNegocio/CapaProcesos/image/Aspirante.png" >
</details>

<details>
<summary>Generacion recibos</summary>
 <img src="CapaNegocio/CapaProcesos/image/GeneracionRecibos.png" >
</details>

<details>
<summary>Calificacion</summary>
<img src="CapaNegocio/CapaProcesos/image/Calificaciones.png" >
</details>

<details>
<summary>Reliquidacion</summary>
<img src="CapaNegocio/CapaProcesos/image/Reliquidacion.png" >
</details>

<details>
<summary>Reportar pago</summary>
<img src="CapaNegocio/CapaCooperacion/image/ViewPointCoop.png" >
</details>

<details>
<summary>ver historia academica</summary>
<img src="CapaNegocio/CapaFunciones/image/HistoriaAcademica.png" >
</details>

<details>
<summary>Movilidad academica</summary>
<img src="CapaNegocio/CapaProcesos/image/MovilidadAcademica.png" >
</details>

<details>
<summary>Graduacion</summary>
<img src="CapaNegocio/CapaProcesos/image/Graduacion.png" >
</details>

<details>
<summary>Modificar notas</summary>
<img src="CapaNegocio/CapaProcesos/image/ModificarNotas.png " >
</details>

<details>
<summary>Pagos (No proceso)</summary>
<img src="CapaNegocio/CapaProcesos/image/Pagos.png" >
</details>

<details>
<summary>Reingresos</summary>
<img src="CapaNegocio/CapaProcesos/image/Reingresos.png" >
</details>

<details>
<summary>Congelados</summary>
<img src="CapaNegocio/CapaProcesos/image/Congelados.png" >
</details>

<details>
<summary>Eliminados</summary>
<img src="CapaNegocio/CapaProcesos/image/Eliminados.png" >
</details>

<details>
<summary>Ver notas</summary>
<img src="CapaNegocio/CapaProcesos/image/VerNotas.png" >
</details>

<details>
<summary>Gestion documentos (No proceso)</summary>
<img src="CapaNegocio/CapaProcesos/image/GestionDocumentos.png" >
</details>

<details>
<summary>Evaluacion docente (No proceso)</summary>
<<img src="CapaNegocio/CapaProcesos/image/EvaluacionDocente.png" >
</details>

<details>
<summary>Inscripcion casos especiales</summary>
<img src="CapaNegocio/CapaProcesos/image/inscribir_condiciones_especiales.png" >
</details>

<details>
<summary>Soporte documental</summary>
<img src="CapaNegocio/CapaProcesos/image/gestion-documental.png" >
</details>

<details>
<summary>Monitoria</summary>
<img src="CapaNegocio/CapaProcesos/image/monitoria.png" >
</details>

<details>
<summary>Evaluar fraude</summary>
<img src="CapaNegocio/CapaProcesos/image/fraude.png" >
</details>

<details>
<summary>Inscripcion</summary>
<img src="CapaNegocio/CapaProcesos/image/Inscripcion.png" >
</details>

<details>
<summary>Preinscripcion</summary>
<img src="CapaNegocio/CapaProcesos/image/pre-inscripcion.png" >
</details>

<details>
<summary>Gestion calendario</summary>
<img src="CapaNegocio/CapaProcesos/image/calendario.png" >
</details>

</details>

---

</details>

---

<details>
 <summary><h4>Modelos de Aplicacion</summary>

<details>
 <summary><h5>Modelo de Vista de Estructura</summary>

---

<details>
<summary>Admitidos</summary>
<img src="CapaAplicacion/CapaEstructura/image/Admitidos.png" >
</details>

<details>
<summary>Gestion documental</summary>
<img src="CapaAplicacion/CapaEstructura/image/GestionDocumental.png" >
</details>

<details>
<summary>Evaluar fraude</summary>
<img src="CapaAplicacion/CapaEstructura/image/fraude.png" >
</details>

<details>
<summary>Inscripcion</summary>
<img src="CapaAplicacion/CapaEstructura/image/Inscripcion.png" >
</details>


</details>

---

<details>
<summary><h5>Modelo de Vista de Uso</summary>

<details>
<summary>Aspirante y Admitidos</summary>
<img src="CapaAplicacion/CapaUso/image/Admitidos.png" >
</details>

<details>
<summary>Gestion documental</summary>
<img src="CapaAplicacion/CapaUso/image/documental.png" >
</details>

<details>
<summary>Evaluar fraude</summary>
<img src="CapaAplicacion/CapaUso/image/fraude.png" >
</details>

<details>
<summary>Inscripcion</summary>
<img src="CapaAplicacion/CapaUso/image/Inscripcion.png" >
</details>

</details>

---

</details>

---

<details>
<summary><h4>Modelo de Infraestructura</h4></summary>
<img src="CapaInfraestructura/image/Uso.png" >
</details>

---


### Licencia

GNU GPL 3.0

Copyrigth Licencia Universidad Distrital Francisco José de Caldas
